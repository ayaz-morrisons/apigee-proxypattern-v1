// proxyName parameter is mandatory
var proxyName = context.getVariable('flow.morrisons.proxyName');

if (proxyName == null || proxyName == ''  || proxyName != "value") {
    context.setVariable('flow.morrisons.error.type','Bad Request');
    context.setVariable('flow.morrisons.httpResponseCode', '400');
    context.setVariable('flow.morrisons.errorCode', '400.14.001');
    context.setVariable('flow.morrisons.reasonPhrase', 'Bad Request');
    context.setVariable('flow.morrisons.errorMessage', 'Proxy Name is invalid');
    context.setVariable('flow.morrisons.errorMoreInfo', 'http://developer.morrisons.com/docs/errors');
    throw new Error();
}