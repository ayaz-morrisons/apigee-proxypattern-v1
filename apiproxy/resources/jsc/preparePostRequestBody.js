/*
Use this snippit if the NB request content-type is JSON only and skip the ExtractVariable policy before this JS

var requestContent = context.getVariable('request.content');
var obj = JSON.parse(requestContent);
*/

var id = context.getVariable('flow.morrisons.id');

var requestJSON = {};
requestJSON.id = id;
context.setVariable('flow.morrisons.requestJSON',JSON.stringify(requestJSON));