var responseContent = context.getVariable('response.content');
var responseJson = JSON.parse(responseContent);

var newResponseJson = {};
newResponseJson.joke = responseJson.value;

context.setVariable('response.content', JSON.stringify(newResponseJson));