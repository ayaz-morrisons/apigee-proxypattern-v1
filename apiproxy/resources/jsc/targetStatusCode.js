switch(context.getVariable("response.status.code"))
	{
		case 503:
			context.setVariable("flow.morrisons.errorCode","503.14.001");
			break;
		case 500:
			context.setVariable("flow.morrisons.errorCode","500.14.001");
			break;
		case 400:
			context.setVariable("flow.morrisons.errorCode","400.14.001");			
			break;
		case 404:
			context.setVariable("flow.morrisons.errorCode","404.14.001");
			break;
		case 401:
			context.setVariable("flow.morrisons.errorCode","401.14.001");
			break;
		case 405:
			context.setVariable("flow.morrisons.errorCode","405.14.001");
			break;
					
		default:
			context.setVariable("flow.morrisons.errorCode","500.14.001");
	}	
var response = context.getVariable("response");
var responseContentLength = context.getVariable('response.header.Content-Length');
try{
    var responseJson = JSON.parse(response.content);
    var isResponseInJson = true;
    response = responseJson;
}
catch(e){}

if(isResponseInJson){
    if((responseJson.length===undefined) && (responseJson.message!==undefined)){
		context.setVariable("flow.morrisons.errorMessage",responseJson.message);
	}else if (responseJson.length==1){
		context.setVariable("flow.morrisons.errorMessage",responseJson[0].message);
	}        
}else if(response.content!==null && response.content!=='' && responseContentLength>0){
    context.setVariable("flow.morrisons.errorMessage",response.content);
}else{
    context.setVariable("flow.morrisons.errorMessage",context.getVariable("response.reason.phrase"));
}

context.setVariable("flow.morrisons.httpResponseCode",context.getVariable("response.status.code"));
context.setVariable("flow.morrisons.reasonPhrase",context.getVariable("response.reason.phrase"));
context.setVariable("flow.morrisons.errorMoreInfo","http://developer.morrisons.com/docs/errors");


