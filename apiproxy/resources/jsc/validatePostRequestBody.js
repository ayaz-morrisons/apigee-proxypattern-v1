/*
Use this snippit if the NB request content-type is JSON only and skip the ExtractVariable policy before this JS

var requestContent = context.getVariable('request.content');
var obj = JSON.parse(requestContent);
*/

var id = context.getVariable('flow.morrisons.id');

if (id != "Proxytemplate Post") {
    context.setVariable('flow.morrisons.error.type','Bad Request');
    context.setVariable('flow.morrisons.httpResponseCode', '400');
    context.setVariable('flow.morrisons.errorCode', '400.14.001');
    context.setVariable('flow.morrisons.reasonPhrase', 'Bad Request');
    context.setVariable('flow.morrisons.errorMessage', 'Invalid body content');
    context.setVariable('flow.morrisons.errorMoreInfo', 'http://developer.morrisons.com/docs/errors');
    throw new Error();
}

