var userAccessToken = context.getVariable("flow.morrisons.userAccessToken");
if (userAccessToken == null || userAccessToken == '') {
	context.setVariable('flow.morrisons.httpResponseCode', '401');
	context.setVariable('flow.morrisons.errorCode', '401.14.001');
	context.setVariable('flow.morrisons.reasonPhrase', 'Unauthorized');
	context.setVariable('flow.morrisons.errorMessage', 'Access token is invalid');
	context.setVariable('flow.morrisons.errorMoreInfo', 'http://developer.morrisons.com/docs/errors');
	throw new Error();
}

var responseCode = context.getVariable("flow.morrisons.userAuthorizationResponse.status.code");

if (responseCode == 200) {
	var responseJson = JSON.parse(context.getVariable("flow.morrisons.userAuthorizationResponse.content"));
	context.setVariable('request.header.Authorization', responseJson.privateToken);
}
else if (responseCode == 404) {
	context.setVariable('flow.morrisons.httpResponseCode', '401');
	context.setVariable('flow.morrisons.errorCode', '401.14.001');
	context.setVariable('flow.morrisons.reasonPhrase', 'Unauthorized');
	context.setVariable('flow.morrisons.errorMessage', 'Access token is invalid');
	context.setVariable('flow.morrisons.errorMoreInfo', 'http://developer.morrisons.com/docs/errors');
	throw new Error();
}
else {
	context.setVariable('flow.morrisons.httpResponseCode', '500');
	context.setVariable('flow.morrisons.errorCode', '500.14.001');
	context.setVariable('flow.morrisons.reasonPhrase', 'Internal Server Error');
	context.setVariable('flow.morrisons.errorMessage', 'Internal Server Error');
	context.setVariable('flow.morrisons.errorMoreInfo', 'http://developer.morrisons.com/docs/errors');
	throw new Error();
}