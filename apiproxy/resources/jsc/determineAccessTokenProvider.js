var userAccessToken = context.getVariable('flow.morrisons.userAccessToken');
if (userAccessToken == null || userAccessToken == '') {
	context.setVariable('flow.morrisons.httpResponseCode', '401');
	context.setVariable('flow.morrisons.errorCode', '401.01.007');
	context.setVariable('flow.morrisons.reasonPhrase', 'Unauthorized');
	context.setVariable('flow.morrisons.errorMessage', 'Access token is invalid');
	context.setVariable('flow.morrisons.errorMoreInfo', 'http://developer.morrisons.com/docs/errors');
	throw new Error();
}

if (userAccessToken.length > 90) {
	context.setVariable('flow.morrisons.auth.accesstoken.provider', 'userv1');
}
else {
	context.setVariable('flow.morrisons.auth.accesstoken.provider', 'userv2');
}